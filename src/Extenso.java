/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aline
 */
public class Extenso implements Trans_extenso {
    
private int numero  = 0, resto1, resto2, div1, div2;


   String centena = "", dezena = "", unidade = "";



    @Override
    public String formatarNumero(int numero) {
      //  System.out.println(" informe um numero: ");
      //  numero=ler.nextInt();
        if (numero <= 1000 && numero != -1) {
            resto1 = numero % 100;
            div1 = numero / 100;
            resto2 = resto1 % 10;
            div2 = resto1 / 10;

            switch (div1) {
                case 1:
                    centena = "cento";
                    break;
                case 2:
                    centena = "duzentos";
                    break;
                case 3:
                    centena = "trezentos";
                    break;
                case 4:
                    centena = "quatrocentos";
                    break;
                case 5:
                    centena = "quinhentos";
                    break;
                case 6:
                    centena = "seiscentos";
                    break;
                case 7:
                    centena = "setecentos";
                    break;
                case 8:
                    centena = "oitocentos";
                    break;
                case 9:
                    centena = "novecentos";
                    break;
            }

            if (resto1 != 0 && resto1 > 10 && resto1 < 20) {
                switch (resto1) {
                    case 11:
                        dezena = "onze";
                        break;
                    case 12:
                        dezena = "doze";
                        break;
                    case 13:
                        dezena = "treze";
                        break;
                    case 14:
                        dezena = "quatorze";
                        break;
                    case 15:
                        dezena = "quinze";
                        break;
                    case 16:
                        dezena = "dezesseis";
                        break;
                    case 17:
                        dezena = "dezessete";
                        break;
                    case 18:
                        dezena = "dezoito";
                        break;
                    case 19:
                        dezena = "dezenove";
                        break;
                }
            } else {
                switch (div2) {
                    case 1:
                        dezena = "dez";
                        break;
                    case 2:
                        dezena = "vinte";
                        break;
                    case 3:
                        dezena = "trinta";
                        break;
                    case 4:
                        dezena = "quarenta";
                        break;
                    case 5:
                        dezena = "cinquenta";
                        break;
                    case 6:
                        dezena = "sessenta";
                        break;
                    case 7:
                        dezena = "setenta";
                        break;
                    case 8:
                        dezena = "oitenta";
                        break;
                    case 9:
                        dezena = "noventa";
                        break;
                }
            }

            if (resto1 >= 20 || numero < 10 || resto1 < 10) {
                switch (resto2) {
                    case 1:
                        unidade = "um";
                        break;
                    case 2:
                        unidade = "dois";
                        break;
                    case 3:
                        unidade = "tres";
                        break;
                    case 4:
                        unidade = "quatro";
                        break;
                    case 5:
                        unidade = "cinco";
                        break;
                    case 6:
                        unidade = "seis";
                        break;
                    case 7:
                        unidade = "sete";
                        break;
                    case 8:
                        unidade = "oito";
                        break;
                    case 9:
                        unidade = "nove";
                        break;
                }
            }

            if (numero == 0) {
                System.out.println("zero");
            } else {
                if (numero == 1000) {
                    System.out.println("mil");
                } else {
                    if (numero == 100) {
                        System.out.println("cem");
                    } else {
                        if (numero > 100 && resto1 < 10 && resto1 != 0) {
                           System.out.println(centena + " e " + unidade);
                        } else {
                           if (numero > 100 && resto1 == 0) {
                                System.out.println(centena);
                            } else {
                                if (numero > 100 && resto1 != 0 && resto2 != 0 && resto1 >= 20) {
                                    System.out.println(centena + " e " + dezena + " e " + unidade);
                                } else {
                                    if (numero > 100 && resto1 != 0 && resto2 == 0) {
                                        System.out.println(centena + " e " + dezena);
                                    } else {
                                        if (numero > 100 && resto1 != 0) {
                                            System.out.println(centena + " e " + dezena);
                                        } else {
                                            if (numero < 100 && resto1 != 0 && resto2 == 0) {
                                                System.out.println(dezena);
                                            } else {
                                                if (numero > 20 && resto1 != 0 && resto2 != 0) {
                                                    System.out.println(dezena + " e " + unidade);
                                                } else {
                                                    if (numero < 10) {
                                                        System.out.println(unidade);
                                                    } else {
                                                        System.out.println(dezena);
                                                    }
                                                }
                                            }
                                       }
                                    }
                                }
                           }
                        }
                   }
                }
            }

        }else if(numero>1000){
            System.out.println("Numero fora dos parametros para escrita");
        }
        return String.valueOf(numero);
    }
}
